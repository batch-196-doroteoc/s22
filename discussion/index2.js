console.log("Hello, Cami!");
let koponanNiEugene = ["Eugene"];
console.log(koponanNiEugene);
koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);
console.log(koponanNiEugene.push("Dennis"));
console.log(koponanNiEugene);
///////////////////////////
let removedItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removedItem);
///////////////////////////
let fruits = ["Mango", "Kiwi", "Apple"];
fruits.unshift("Pineapple");
console.log(fruits);
///////////////////////////////////
let computerBrands = ["Apple", "Acer", "Asus", "Dell"];
console.log(computerBrands);
computerBrands.shift();
console.log(computerBrands);
///////////////////////////////////
computerBrands.splice(1);
console.log(computerBrands);
/////////////////////////////////
fruits.splice(0,1);
console.log(fruits);
//////////////////////////
koponanNiEugene.splice(1,0,"Dennis","Alfred");
console.log(koponanNiEugene);
///////////////////
fruits.splice(0,2,"Lime","Cherry");
console.log(fruits);
////////////////////
let item = fruits.splice(0);
console.log(fruits);
console.log(item);
///////////////////
let spiritDetective = koponanNiEugene.splice(0,1);
console.log(spiritDetective);
console.log(koponanNiEugene);
//////////////////
let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];
members.sort();
console.log(members);
//////////////////
let numbers = [50,100,12,10,1];
numbers.sort();
console.log(numbers);
///////////////////
members.reverse();
console.log(members);
//////////////////
let carBrands = ["Vios", "Fortuner", "Crosswind","City","Vios", "Starex"];
let firstIndexofVios = carBrands.indexOf("Vios");
console.log(firstIndexofVios); //returns 0
//very useful in finding the index of the array if the lentght is unknwon ad if the array is constant;y being added into or manipulated
let indexOfStarex = carBrands.indexOf("Starex");
console.log(indexOfStarex); //returns 5
//when the argument passed does not match any item in the array, -1 will be returned
let indexOfBeetle = carBrands.indexOf("Beetle");
console.log(indexOfBeetle); //returns -1
//lastIndexOf()
//Returns the index number of the last matching element in the array
let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfVios);//4
let indexOfMio = carBrands.lastIndexOf("Mio");
console.log(indexOfMio);
////////////////
let shoeBrand = ["Jordan", "Nike", "Adidas", "Converse", "Sketchers"];
//slice (startingIndex) = allows us to copy an array into a new array with items from the startingIndex to the last item.
let myOwnedShoes = shoeBrand.slice(1);
console.log(myOwnedShoes);
//non mutator methods do not update the original array
console.log(shoeBrand);
//slice(startingIndex,endingIndex) = allows us to copy an array into a new array with items from the starting index to just before the endingIndex
//from starting to the 1 item before the ending index
let herOwnedShoes = shoeBrand.slice(2,4);
console.log(herOwnedShoes);
console.log(shoeBrand);
/////////////////////
let heroes = ["Captain America","Superman","Spiderman","Wonder Woman", "Hulk", "Hawkeye", "Dr. Strange"];

let myFavoriteHeroes = heroes.slice(2,6);
console.log(myFavoriteHeroes);
console.log(heroes);
////////////////
let superHeroes = heroes.toString();
console.log(superHeroes);
console.log("My favorite heroes are " + superHeroes);

//join()
//returns our arrays as a string by specified seprator

//without a specified separator - it defaults into a separate elements with comma
let superHeroes2 = heroes.join();
console.log(superHeroes2);

let superHeroes3 = heroes.join(" ");
console.log(superHeroes3);

let superHeroes4 = heroes.join(1);
console.log(superHeroes4);
//////////////
let counter = 0;

heroes.forEach(function(hero){
	counter++;
	console.log(counter)
	console.log(hero);
})

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

chessBoard.forEach(function(row){
	// console.log(row);
	row.forEach(function(square){
		console.log(square);
	});
})

let numArr = [5,12,30,46,40];

numArr.forEach(function(number){
	if(number%5===0){
		console.log(number+" is divisible by 5");
	}else{
		console.log(number+ " is not divisible by 5")
	}

})

let instructors = members.map(function(member){

	return member + " is an instructor";
})

console.log(instructors);
//map will return a new array 
//map() returns a new array which contains the value/data returned by the function that was for each item in the original array
console.log(members);

let numArr2 = [1,2,3,4,5];
let squareMap = numArr2.map(function(number){
		return number * number
})

console.log(squareMap);
console.log(numArr2);

let squareForEach = numArr2.forEach(function(number){

	return number*number
})

console.log(squareForEach);

let isAMember = members.includes("Tine")
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);